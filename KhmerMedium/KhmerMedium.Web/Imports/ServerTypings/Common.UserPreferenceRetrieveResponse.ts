﻿namespace KhmerMedium.Common {
    export interface UserPreferenceRetrieveResponse extends Serenity.ServiceResponse {
        Value?: string;
    }
}

