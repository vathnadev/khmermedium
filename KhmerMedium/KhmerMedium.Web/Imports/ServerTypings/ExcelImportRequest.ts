﻿namespace KhmerMedium {
    export interface ExcelImportRequest extends Serenity.ServiceRequest {
        FileName?: string;
    }
}

